var appName='GreenArrow';
var jsPath='./lib/js/';
var modulesPath='./modules/';
var cssPath='./lib/assets/css/';
var directivePath='./lib/template/';
var greenArrowApp = angular.module('greenArrow', ['ui.bootstrap']);
var bottomNavBar=angular.module('bottom-navbar',['pascalprecht.translate']);
var menuBuilder=require(modulesPath+'menu/menu');
var gui =global.window.nwDispatcher.requireNwGui();
var top=menuBuilder.getInstance('top','pt',[]).buildMenu();


greenArrowApp.controller('personCtrl', function($scope) {
	$scope.firstName = "John";
	$scope.lastName = "Doe";
	$scope.fullName = function() {
		return $scope.firstName + " " + $scope.lastName;
	};
	
});


//bootstrapModules

angular.module('main',['greenArrow',
					   'bottom-navbar'
                       ]);
