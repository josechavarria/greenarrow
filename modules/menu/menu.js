

exports.Menu=function(wich,_appLang,callback){
possible=['bottom_navbar','top','left','right']
	 
if(possible.indexOf(wich) < 0) {return null;}
	;
	var _wich=wich;
	var appLang=_appLang;
	var gui =global.window.nwDispatcher.requireNwGui();
	var url = './'+_wich+'_menu.json';
	var menuActions=new (require('./menuActions')).actions(gui.Window.get());
	//para o cms criar a arvore de diretorios
	fs = require('fs');
	var DIR1='./locales/';
	var DIR2='menu/';
	var DIR3='top/';
	if (!fs.existsSync(DIR1)){
	    fs.mkdirSync(DIR1);
	}
	if (!fs.existsSync(DIR1+DIR2)){
	    fs.mkdirSync(DIR1+DIR2);
	}
	if (!fs.existsSync(DIR1+DIR2+DIR3)){
	    fs.mkdirSync(DIR1+DIR2+DIR3);
	}
	var  i18n = new (require("i18n-2"))({
		locales:['pt', 'en'],
		directory: DIR1+DIR2+DIR3,
		extension: '.json'
	});
	i18n.setLocale(appLang);
		this.translate=function(_cmsKeyId){
		return i18n.__(_cmsKeyId);
		};
		this.getConfigTree=function(){
			var config = require(url);
			return config;	
		};
		this.buildMenu=function(){
			if(_wich==='top'){
				this.buildTopMenu();
			}else{
				console.log('Other menus not implemented :'+_wich);
			}
		};
		this.buildTopMenu=function(){
			var menu;
			var tree=this.getConfigTree();
			if(tree.items!==null&&tree.items!==undefined){
				menu=this.extractFromNodeTreeMenu(tree,{type:'menubar'});
			}
			if(menu!==null && menu!==undefined){
				gui.Window.get().menu=menu;
			}
		};
		this.extractFromNodeTreeMenu=function(node,_arguments){
			
			var _menu;
			if(_arguments.type!==undefined){
				_menu=new gui.Menu(_arguments);
			}else
			{
				_menu=new gui.Menu();
			}
					
			var _items=node.items;
			for(var i in _items){
				var n=_items[i];
				var _subMenu={ icon:(n.icon!==undefined&&n.icon!==null) ? n.icon :'',
						label: this.translate(n.cmsKeyid), id:n.id};
						if(n.action!==undefined
								&&n.action!==null
								&&n.action!==""&&
								menuActions[n.action]!==undefined)
										{
											_subMenu.click=menuActions[n.action]; 
										}						

						if(n.items!=undefined && n.items !=null){
							_subMenu.submenu=this.extractFromNodeTreeMenu(n,[]);
						}
				_menu.append(new gui.MenuItem(_subMenu));
			}
			
			
			return _menu;
			
		};
		this.actionNotImplementedYet=function(name){
			console.log("Method/function with name :"+name+" was not implemented yet");
		};
		
		if(typeof callback=== "function"){
			callback();
		}
		
		
		
}

 exports.getInstance= function(_wichMenu,_appLang,_callback){
	 	var _wichMenuThat=_wichMenu;
	 	var appLang=_appLang;
	 	var callback=_callback;
		return new this.Menu(_wichMenuThat,appLang,callback);
	};


