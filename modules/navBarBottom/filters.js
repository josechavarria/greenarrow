.directive('tree',function($compile){
	return {
        restrict: "E",
        transclude: true,
        scope: {menutree: '='},
       //template:       
         //  '<ul>'
           //     '<li ng-transclude></li>' +
             //   '<li ng-repeat="child in family.children">' +
               //     '<tree family="child">{{family.name}}</tree>' +
               // '</li>' +
            //'</ul>','
         template:'<li ng-repeat="item in bottom" ng-if="(items===undefined||items.length===0)">'
            + '<a href="{{(action!==undefined&&action!==\'\'?action:\'#\'"}}">'
            +'<span ng-if="(icon!=undefined&&icon!==null&&icon!=\'\')"/>{{cmsKeyId | translate}}</a></li>',
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone); 
                });
            };
        }
    };
	
});