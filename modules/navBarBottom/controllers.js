//var menuBuilder=require(modulesPath+'menu/menu');

bottomNavBar.value('menuBuilder',menuBuilder);
bottomNavBar

.controller('bottom-navbar-controller',['$scope','menuBuilder',function($scope,menuBuilder) {
	 $scope.bottom=menuBuilder.getInstance('bottom_navbar','pt',[]).getConfigTree();
	 $scope.appName="GreenArrow";
	 $scope.items=$scope.bottom.items;
	// $scope.setLang = function(langKey) {
		   
		 
//}
	
}]).filter('itemsContentFilter', function(){ return function(item) {
		return (item.items!==undefined && item.items!==null&&item.items.length>0);
  }}).filter('itemsContentFilterDeny',function(){ return function(item) {
		return (item.items===undefined && item.items===null&&item.items.length<=0);
  }})
.directive('tree',function($compile){

	return {
        restrict: "E",
        transclude: true,
        scope: {menutree: '='},
        templateUrl:'modules/navBarBottom/main-template-item.html',
        compile: function(tElement, tAttr, transclude) {
        
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone); 
                });
            };
        }
    };
	
})